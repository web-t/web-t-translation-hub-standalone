## WEB-T Universal plugin translation hub Docker compose for local launch and usage

#### This repository contains Docker compose file, which includes all the services needed for launch and usage of WEB-T Universal plugin translation hub.

It contains following services:
* Frontend
* Crawler agent for website pre-translation
* Website translation service
* Configuration service
* MT API integrator
* ETranslation Provider
* Ngrok (optional, alternatives of Ngrok also can be used; required for use of eTranslation provider locally)
* MySQL database
* Redis
* Traefik proxy

:exclamation: To launch and use this container locally, machine needs Docker Desktop installed on it. :exclamation:

:exclamation: Due to asynchronous workflow of eTranslation API, translation with eTranslation provider will not work unless [eTranslation provider component](https://code.europa.eu/web-t/web-t-translation-hub-containerable/etranslation-provider) is published online and accessible from eTranslation API via HTTPS. For local setup you can try localhost tunneling solutions (e.g. ngrok, Pagekite) or use Custom provider. :exclamation:

- - -

#### How to launch?

In the folder where docker-compose.yaml is located, open terminal.
In terminal, the following command has to be run.
```docker-compose up```

To check whether container and all services are up and running, it can be done in [Docker Desktop](https://docs.docker.com/desktop/install/windows-install/)
![alt text](image.png) 
or in terminal, with command
```docker ps -a```

If docker build was successful and all services are up and running, then in browser address bar input 
```webt-translation-hub.localhost``` or open http://webt-translation-hub.localhost/

It should launch functioning WEB-T translation hub.

- - - -

#### How to use translation with eTranslation provider locally?

##### With ngrok

First, sign up at https://ngrok.com/. After sign up, on main page you can find multiple instructions about how to use ngrok in various ways. We recommend running ngrok together with other containers using Docker compose, but you can also run it separately.
###### Run ngrok with Docker compose (https://dashboard.ngrok.com/get-started/setup/docker)
1. In docker-compose.yaml file uncomment Configuration__PublicUri in etranslation-provider as well as whole ngrok service.
2. Replace YOUR-NGROK-AUTH-TOKEN with your ngrok token.
3. If that has not been done yet, in https://dashboard.ngrok.com/cloud-edge/domains generate a static domain (in free plan one such domain can be generated). Format of the domain is *.ngrok-free.app
4. Replace YOUR-NGROK-DOMAIN with this generated domain (both in ngrok command and for Configuration__PublicUri)
NOTICE! - for Configuration__PublicUri it is important to add https:// in front of your generated domain, for example,
https://random-domain.ngrok-free.app/
###### Run ngrok as a separate process (https://dashboard.ngrok.com/get-started/setup/windows).
1. After installing ngrok on windows and adding your auth token, launch this command in windows command prompt
```ngrok http http://localhost:5079```
Why port 5079? On this port, according to docker-compose, is exposed eTranslation provider.
2. ngrok will generate a random public url for this local service (format *.ngrok-free.app), copy it.
3. In docker-compose.yaml, etranslation-provider there is Configuration__PublicUri, which currently is commented out. Uncomment it and in paste ngrok url in between quotes.

- - - -

#### Notes

* Value ```webt-translation-hub.localhost``` can be replaced with any domain, locally it should end with ```.localhost``` (more about Traefik Routers https://doc.traefik.io/traefik/routing/routers/).
Otherwise, Traefik supports any configured domains, as well as can provide TLS (https://doc.traefik.io/traefik/https/overview/).
* ```website-translation-service``` and ```configuration-service``` use MySQL and following connection string
"server=mysql;database=DATABASE_NAME;user=root;password=root"
User and Password should be changed if launched to production, since locally it uses simple, default values.
* ```mysql``` service is using MYSQL_ROOT_PASSWORD: root. It can also be changed.
* If you wish to build any of services not from gitlab link, but from local repository, it is possible to change any of build -> context links to local folder path.
For example, if website-translation-service repository is located in C:/Test/Path/website-translation-service, then respective build context and dockerfile path can be changed to
```
build: 
    context: C:/Test/Path/website-translation-service
    dockerfile: ./WebsiteTranslationService/Dockerfile
```
* For more advanced deployment setup and configuration you can check this repository - https://code.europa.eu/web-t/web-t-translation-hub-containerable/deployment/-/tree/main/translation-hub